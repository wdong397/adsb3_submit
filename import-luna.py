#!/usr/bin/env python
import sys
import json
import subprocess
from multiprocessing import Pool
from adsb3 import *
import cv2
from tqdm import tqdm
import picpac

MAX = -1 #100 #None #50

def dump (db, case):
    images = case.images
    N, H, W = images.shape
    by_frame = {}
    neg = set(range(N))
    for anno in case.picpac_anno():
        for j, x, y, rx, ry in anno:
            r = max(W * rx, H * ry) #* SPACING
            print '\t', j, x, y, rx, ry, r
            if r < 3:   # less than 
                continue
            try:
                neg.remove(j)
            except:
                pass
            one = by_frame.setdefault(j, [])
            one.append({
                    'type': 'ellipse',
                    'geometry': {
                        'x': x - rx,
                        'y': y - ry,
                        'width': rx * 2,
                        'height': ry * 2
                    }
                })
            pass
        pass
    for j, shapes in by_frame.iteritems():
        image = get3c(images, j)
        if image is None:
            continue
        anno = {"shapes": shapes}
        buf1 = cv2.imencode('.png', image)[1].tostring()
        buf2 = json.dumps(anno)
        db.append(buf1, buf2)
    pass

#subprocess.check_call('rm -rf db/luna3c/*', shell=True)
try_mkdir('db/luna')
try_remove('db/luna/axial')
try_remove('db/luna/sagittal')
try_remove('db/luna/coronal')
db1 = picpac.Writer('db/luna/axial')
db2 = picpac.Writer('db/luna/sagittal')
db3 = picpac.Writer('db/luna/coronal')
uids = LUNA_ANNO.keys()

print 'CASES:', len(uids)
pool = Pool(None)
for uid in tqdm(uids):
    case = load_8bit_lungs_noseg(uid)
    case = case.rescale3D(SPACING)
    dump(db1, case)
    dump(db2, case.transpose(SAGITTAL))
    dump(db3, case.transpose(CORONAL))
    pass

