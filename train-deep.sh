#!/bin/bash

# About training with picpac
# Typicall two databases are used for training:
#   1. database with positive regions (LUNA), specified with --db
#   2. database with pure negatives (BOWL slices of negative training images of stage1), specified with --mixin
# -net specifies the network architecture.

# nodule models are trained with databases of matching view
# feature models are always trained with axial view.

# train unet_k
mkdir -p train/unet_k
for V in axial
do
./fcn-train.py --db db/luna/$V --mixin db/bg/$V  --model train/unet_k/$V --net mytinyunet4
mkdir -p models2/unet_k/$V
cp train/unet_k/$V/200000.* models2/unet_k/$V
done

# train unet
mkdir -p train/unet
for V in axial coronal sagittal
do
./fcn-train.py --db db/luna/$V --mixin db/bg/$V --net mytinyunet4 --model train/unet/$V --nopert_hflip --nopert_vflip
mkdir -p models2/unet/$V
cp train/unet/$V/200000.* models2/unet/$V
done

# train tiny
mkdir -p train/tiny
for V in axial coronal sagittal
do
./fcn-train-old.py --db db/luna/$V --mixin db/bg/$V --net tiny --model train/tiny/$V --cache 0 --channels 3
mkdir -p models2/tiny/$V
cp train/tiny/$V/200000.* models2/tiny/$V
done

# train ft
mkdir -p train/ft
./fcn-ft-train.py --db db/tcia/axial --mixin db/bg/axial  --model train/ft --net tinyZ
mkdir -p models2/ft
cp train/ft/200000.* models2/ft/

# train ft1
mkdir -p train/ft1
./fcn-ft-train-old.py --db db/tcia/axial --mixin db/bg/axial  --model train/ft1 --net tinyX2
mkdir -p models2/ft1
cp train/ft1/200000.* models2/ft1/

