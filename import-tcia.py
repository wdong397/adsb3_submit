#!/usr/bin/env python
import sys
from glob import glob
import xml.etree.ElementTree as ET
import numpy as np
import logging
import cv2
import simplejson as json
import tcia
import picpac
from adsb3 import *

RADIUS = 4

def encode_3 (a, b, c):
    return (a * 6 + b) * 6 + c

def encode_ft (ft):
    lab = 1
    sbt, ins, cal, sph, mgn, lob, spi, tex, mal = ft
    # sbt   1-5
    # sph   2-5
    # mgn   1-5
    # lob   1-5
    # spi   1-5
    # tex   1-5
    # mal   1-5
    cal1 = 0
    cal2 = 0
    if cal == 6:
        pass
    if cal == 3 or cal == 5:    # solid
        cal1 = 1
    else:
        cal2 = 1
    return [encode_3(sbt, sph, mgn), encode_3(lob, spi, tex), encode_3(mal, cal1, cal2)]

logging.basicConfig(level=logging.INFO)
#for path in glob('data/tcia/tcia-lidc-xml/185/069.xml'):
try_mkdir('db/tcia/')
try_remove('db/tcia/axial')
db = picpac.Writer('db/tcia/axial')
C = 0
paths = sys.argv[1:]
if len(paths) == 0:
    paths = glob('data/tcia/tcia-lidc-xml/*/*.xml')
for path in paths:
    print path
    anno = tcia.AnnotationSessions(path)
    if len(anno.sessions) == 0:
        continue
    try:
        #case = Case(anno.uid, regroup=False)
        case = LunaCase(anno.uid)
    except:
        print 'failed to load, trying luna', anno.uid
        continue
        #case = LunaCase(anno.uid)
        continue
    case.standardize_color()
    _, H, W = case.images.shape
    case = case.rescale3D(SPACING)

    for rois in anno.sessions:
        for ft, nodule in rois:
            nodule = sorted(nodule, key=lambda x: x[0])
            label = encode_ft(ft)
            print len(nodule), label, ft
            z, pts = nodule[len(nodule)/2]
            i = int(round((z - case.origin[0]) / case.spacing[0]))
            image = get3c(case.images, i)
            if  image is None:
                continue
            points = []
            for x, y in pts:
                points.append({'x': 1.0 * x / W,
                               'y': 1.0 * y / H})
                pass
            anno = {'shapes':[{'type':'polygon', 'label': label, 'geometry':{'points': points}}]}
            buf1 = cv2.imencode('.png', image)[1].tostring()
            buf2 = json.dumps(anno)
            db.append(buf1, buf2)
        pass
    C += 1
    pass

