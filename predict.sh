#!/bin/bash

# Each of the cache_XXX command below can run in parallel
# on a cluster with shared filesystem.

./cache_mask.py     # generating mask cache
./cache_all_ft.py   # extract deep learning feature 

###########################################################

# Each of the command below will generate a *.submit file
# in stage directory.  The program will print the filename.

./predict1.py
./predict2.py
