import os
import traceback
import xml.etree.ElementTree as ET
import logging
from glob import glob
import cPickle as pickle

FEATURES = ['subtlety', 'internalStructure', 'calcification', 'sphericity', 'margin', 'lobulation', 'spiculation', 'texture', 'malignancy']
FEATURES_SHORT = ['sbt', 'ins', 'cal', 'sph', 'mgn', 'lob', 'spi', 'tex', 'mal']

SESSION_TAGS_LOWER = ['annotationversion', 'servicingradiologistid', 'unblindedread', 'nonnodule']

def tciaxmlget (parent, text):
    text = text.lower()
    for c in parent.getchildren():
        t = c.tag.lower()
        if '}' in t:
            t = t[(t.find('}')+1):]
        if text in t:
            return c
    pass

def tciaxmlgettext (parent, text):
    return tciaxmlget(parent, text).text.strip()

def tciaxmlgettextdefault (parent, text, default):
    try:
        return tciaxmlget(parent, text).text.strip()
    except:
        return default

def tciaxmlgetfloat (parent, text):
    return float(tciaxmlget(parent, text).text.strip())

def tciaxmlgetall (parent, text):
    text = text.lower()
    for c in parent.getchildren():
        t = c.tag.lower()
        if '}' in t:
            t = t[(t.find('}')+1):]
        if text in t:
            yield c
        pass
    pass

def loadFeatures (node):
    ft = []
    for key in FEATURES:
        ft.append(tciaxmlgetfloat(node, key))
        pass
    return ft

with open('data/luna/z.pkl', 'rb') as f:
    z_lookup = pickle.load(f)
#print z_lookup['1.3.6.1.4.1.14519.5.2.1.6279.6001.330516096286629321265359295950']

def loadROI (node):
    sop = None
    z = None
    sop = tciaxmlgettext(node, 'imageSOP')
    try:
        z = tciaxmlgetfloat(node, 'imageZposition')
    except:
        pass
    if not sop:
        raise Exception('missing SOP')
    z_pos = z_lookup.get(sop, None)
    if z is None:
        print 'missing Z', sop
        z = z_pos
    elif not z_pos is None:
        assert z == z_pos
    #if z is None:
    #    z = z_lookup[sop]
    #if sop and z:
    #    logging.warn('having both SOP and ZPOS')
    inc = tciaxmlgettextdefault(node, 'inclusion', 'true').lower()
    assert inc == 'true' or inc == 'false'
    inc = (inc == 'true')
    pts = []
    for em in tciaxmlgetall(node, 'edgeMap'):
        pts.append((tciaxmlgetfloat(em, 'xCoord'), tciaxmlgetfloat(em, 'yCoord')))
        pass
    return z, inc, pts

class SliceAnnotation:
    def __init__ (self):
        self.nodules = []           #  [(ft, roi)]
        self.non_nodules = []       #  [(x,y)]
        self.small_nodules = []     #  [(x,y)]
        pass

def check_session_children (session):
    for c in session.getchildren():
        ct = c.tag.lower()
        good = False
        for ctl in SESSION_TAGS_LOWER:
            if ctl in ct:
                good = True
                break
        if not good:
            logging.error("unseen nod: %s", ET.tostring(c))
        pass

class Annotation:
    def __init__ (self, path):
        root = ET.parse(path).getroot()
        header = tciaxmlget(root, 'responseHeader')
        uid = tciaxmlget(header, 'seriesInstanceUid')
        self.uid = uid.text.strip()
        self.nodules = []   # nodule features
        anno = {}
        nodule_idx = 0
        for session in tciaxmlgetall(root, 'readingSession'):
            check_session_children(session)
            for non in tciaxmlgetall(session, 'nonNodule'):
                locus = tciaxmlget(non, 'locus')
                z = tciaxmlgetfloat(non, 'imageZposition')
                x = tciaxmlgetfloat(locus, 'xCoord')
                y = tciaxmlgetfloat(locus, 'yCoord')
                #print z, x, y
                anno.setdefault(z, SliceAnnotation()).non_nodules.append((x,y))
            for nod in tciaxmlgetall(session, 'unblindedRead'):
                ft = None
                ftnode = tciaxmlget(nod, 'characteristics')
                ft_log = None
                if not ftnode is None:
                    try:
                        ft = loadFeatures(ftnode)
                        ft.append(nodule_idx)
                        nodule_idx += 1
                    except:
                        ft_log = traceback.format_exc()
                        ft = None
                self.nodules.append(ft)
                for roi in tciaxmlgetall(nod, 'roi'):
                    try:
                        z, inc, pts = loadROI(roi)
                    except:
                        logging.error('bad roi: %s' % path)
                        logging.error('bad roi xml: %s' % ET.tostring(roi))
                        logging.error(traceback.format_exc())
                        continue
                    if not z:
                        continue
                    if not inc:
                        continue
                    if len(pts) == 1:
                        anno.setdefault(z, SliceAnnotation()).small_nodules.append(pts[0])
                    else:
                        if ft is None:
                            logging.error('bad ft: %s %s' % (path, ft_log))
                            continue
                        anno.setdefault(z, SliceAnnotation()).nodules.append((ft, pts))
                    pass
                pass
        self.slices = anno

class AnnotationSessions:
    def __init__ (self, path):
        root = ET.parse(path).getroot()
        header = tciaxmlget(root, 'responseHeader')
        uid = tciaxmlget(header, 'seriesInstanceUid')
        self.uid = uid.text.strip()
        self.sessions = []
        for session in tciaxmlgetall(root, 'readingSession'):
            check_session_children(session)
            rois = []   # (ft, [(z, pts)])
            for nod in tciaxmlgetall(session, 'unblindedRead'):
                ft = None
                ftnode = tciaxmlget(nod, 'characteristics')
                ft_log = None
                if not ftnode is None:
                    try:
                        ft = loadFeatures(ftnode)
                    except:
                        ft_log = traceback.format_exc()
                        ft = None
                one = []
                for roi in tciaxmlgetall(nod, 'roi'):
                    try:
                        z, inc, pts = loadROI(roi)
                    except:
                        logging.error('bad roi: %s' % path)
                        logging.error('bad roi xml: %s' % ET.tostring(roi))
                        logging.error(traceback.format_exc())
                        continue
                    if not z:
                        continue
                    if not inc:
                        continue
                    if len(pts) > 1:
                        if ft is None:
                            logging.error('bad ft: %s %s' % (path, ft_log))
                            continue
                        one.append((z, pts))
                    pass
                if len(one) > 0:
                    rois.append((ft, one))
                pass
            if len(rois) > 0:
                self.sessions.append(rois)
            pass
        pass
    pass

class DcmAnnotationSessions:
    def __init__ (self, path):
        p = os.path.dirname(path)
        self.seriesInstanceUid = os.path.basename(p)
        p = os.path.dirname(p)
        self.studyInstanceUid = os.path.basename(p)
        p = os.path.dirname(p)
        self.tciaId = os.path.basename(p)


        root = ET.parse(path).getroot()
        header = tciaxmlget(root, 'responseHeader')
        self.modality = 'CT'
        try:
            self.modality = tciaxmlgettext(header, 'Modality').upper()
        except:
            pass
        if self.modality != 'CT':
            return

        self.uid = tciaxmlgettext(header, 'seriesInstanceUid')
        #print self.uid
        sid = tciaxmlgettext(header, 'studyInstanceUid')
        #print sid
        assert self.uid == self.seriesInstanceUid
        assert sid == self.studyInstanceUid
        self.sessions = []
        for session in tciaxmlgetall(root, 'readingSession'):
            check_session_children(session)
            rois = []   # (ft, [(z, pts)])
            for nod in tciaxmlgetall(session, 'unblindedRead'):
                ft = None
                ftnode = tciaxmlget(nod, 'characteristics')
                ft_log = None
                if not ftnode is None:
                    try:
                        ft = loadFeatures(ftnode)
                    except:
                        ft_log = traceback.format_exc()
                        ft = None
                one = []
                for roi in tciaxmlgetall(nod, 'roi'):
                    try:
                        z, inc, pts = loadROI(roi)
                    except:
                        logging.error('bad roi: %s' % path)
                        logging.error('bad roi xml: %s' % ET.tostring(roi))
                        logging.error(traceback.format_exc())
                        continue
                    if not z:
                        continue
                    if not inc:
                        continue
                    if len(pts) > 1:
                        if ft is None:
                            logging.error('bad ft: %s %s' % (path, ft_log))
                            continue
                        one.append((z, pts))
                    pass
                if len(one) > 0:
                    rois.append((ft, one))
                pass
            if len(rois) > 0:
                self.sessions.append(rois)
            pass
        pass
    pass


def load_xmls ():
    if os.path.exists('data/tcia/xmls.pkl'):
        with open('data/tcia/xmls.pkl', 'rb') as f:
            return pickle.load(f)
    xmls = list(glob('data/lymph/data/*/*/*/*.xml'))
    with open('data/tcia/xmls.pkl', 'wb') as f:
        pickle.dump(xmls, f)
        pass
    return xmls

