#!/usr/bin/env python
import os
from glob import glob
import dicom
import cPickle as pickle

lookup = {}
for D in glob('/data/single/wdong/DOI/LIDC-*'):
    for path in glob(os.path.join(D, "*/*/*.dcm")):
        try:
            dcm = dicom.read_file(path)
            _, _, z = [float(v) for v in dcm.ImagePositionPatient]
            sop = dcm.SOPInstanceUID
            lookup[sop] = z
            print sop, z
        except:
            pass
with open('data/luna/z.pkl', 'wb') as f:
    pickle.dump(lookup, f)
