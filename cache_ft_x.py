#!/usr/bin/env python
import sys
import math
import time
import traceback
import subprocess
import numpy as np
import tensorflow as tf
import Queue
import threading
from tensorflow.python.framework import meta_graph
from scipy.ndimage.morphology import grey_dilation, binary_dilation
from skimage import measure
from adsb3 import *
import pyadsb3

BATCH = 32

def setGpuConfig (config):
    mem_total = subprocess.check_output('nvidia-smi --query-gpu=memory.total --format=csv,noheader,nounits', shell=True)
    mem_total = float(mem_total)
    frac = 5000.0/mem_total
    print("setting GPU memory usage to %f" % frac)
    if frac < 0.5:
        config.gpu_options.per_process_gpu_memory_fraction = frac
    pass

def logits2prob (v, scope='logits2prob'):
    with tf.name_scope(scope):
        shape = tf.shape(v)    # (?, ?, ?, 2)
        # softmax
        v = tf.reshape(v, (-1, 2))
        v = tf.nn.softmax(v)
        v = tf.reshape(v, shape)
        # keep prob of 1 only
        v = tf.slice(v, [0, 0, 0, 1], [-1, -1, -1, -1])
        # remove trailing dimension of 1
        v = tf.squeeze(v, axis=3)
    return v

class ViewModel:
    def __init__ (self, X, KEEP, view, name, dir_path, node='logits:0', softmax=True):
        self.name = name
        self.view = view
        paths = glob(os.path.join(dir_path, '*.meta'))
        assert len(paths) == 1
        path = os.path.splitext(paths[0])[0]
        mg = meta_graph.read_meta_graph_file(path + '.meta')
        if KEEP is None:
            fts, = tf.import_graph_def(mg.graph_def, name=name,
                    input_map={'images:0':X},
                                return_elements=[node])
        else:
            fts, = tf.import_graph_def(mg.graph_def, name=name,
                    input_map={'images:0':X, 'keep:0':KEEP},
                                return_elements=[node])
        if softmax:
            fts = logits2prob(fts)
        self.fts = fts
        self.saver = tf.train.Saver(saver_def=mg.saver_def, name=name)
        self.loader = lambda sess: self.saver.restore(sess, path)
        pass

class Model:
    def __init__ (self, prob_model, fts_model, channels = 3, prob_dropout=False, fts_dropout=True):
        assert not prob_model is None
        assert not fts_model is None
        if channels == 1:
            self.X = tf.placeholder(tf.float32, shape=(None, None, None))
            X4 = tf.expand_dims(self.X, axis=3)
        elif channels == 3:
            self.X = tf.placeholder(tf.float32, shape=(None, None, None, channels))
            X4 = self.X 
        else:
            assert False
        self.KEEP = tf.placeholder(tf.float32, shape=())
        PROB_KEEP = None
        FTS_KEEP = None
        if prob_dropout:
            PROB_KEEP = self.KEEP
        if fts_dropout:
            FTS_KEEP = self.KEEP

        models = []
        models.append(ViewModel(X4, FTS_KEEP, AXIAL, 'fts', 'models/%s' % fts_model, node='fts:0', softmax=False))
        models.append(ViewModel(X4, PROB_KEEP, AXIAL, 'axial', 'models/%s/axial' % prob_model))
        models.append(ViewModel(X4, PROB_KEEP, SAGITTAL, 'sagittal', 'models/%s/sagittal' % prob_model))
        models.append(ViewModel(X4, PROB_KEEP, CORONAL, 'coronal', 'models/%s/coronal' % prob_model))

        self.channels = channels
        self.models = models
        pass

    def load (self, sess):
        for m in self.models:
            if m:
                m.loader(sess)
        pass

    def apply (self, sess, case):
        #comb = np.ones_like(case.images, dtype=np.float32)
        views = [case.transpose(AXIAL)]
        views.append(case.transpose(SAGITTAL))
        views.append(case.transpose(CORONAL))
        r = []
        for m in self.models:
            cc = views[m.view]
            images = cc.images
            N, H, W = images.shape

            fts = None #np.zeros_like(images, dtype=np.float32)
            margin = 0
            if self.channels == 3:
                margin = GAP
            fts = None
            off = margin
            while off < N-margin:
                nxt = min(off + BATCH, N-margin)
                x = np.zeros((nxt-off, H, W, FLAGS.channels), dtype=np.float32)
                i = 0
                for j in range(off, nxt):
                    if self.channels == 1:
                        x[i] = images[j]
                    elif self.channels == 3:
                        x[i,:,:,0] = images[j-GAP]
                        x[i,:,:,1] = images[j]
                        x[i,:,:,2] = images[j+GAP]
                    else:
                        assert False
                    i += 1
                    pass
                assert i == x.shape[0]
                y, = sess.run([m.fts], feed_dict={self.X:x, self.KEEP:1.0})
                if fts is None:
                    fts = np.zeros((N,) + y.shape[1:], dtype=np.float32)
                fts[off:nxt] = y
                off = nxt
                pass
            assert off == N - margin
            if m.view != AXIAL:
                fts = cc.transpose_array(AXIAL, fts)
            r.append(fts) 
            pass
        return r
    pass

def extract_view (ROOT, uid, view, prob, fts, th=0.05, ext=2):
    prob4 = np.reshape(prob, prob.shape + (1,))
    fts = fts * prob4

    binary = prob > th
    k = int(round(ext / SPACING))
    binary = binary_dilation(binary, iterations=k)
    labels = measure.label(binary, background=0)
    boxes = measure.regionprops(labels)

    nodules_a = []
    nodules_b = []
    dim_a = 2
    dim_b = fts.shape[3]

    Z, Y, X = prob.shape
    for box in boxes:
        #print prob.shape, fts.shape
        z0, y0, x0, z1, y1, x1 = box.bbox
        #ft.append((z1-z0)*(y1-y0)*(x1-x0))
        prob_roi = prob[z0:z1,y0:y1,x0:x1]
        za, ya, xa, zz, zy, zx, yy, yx, xx = pyadsb3.norm3d(prob_roi)
        zc = za + z0
        yc = ya + y0
        xc = xa + x0

        cov = np.array([[zz, zy, zx],
                        [zy, yy, yx],
                        [zx, yx, xx]], dtype=np.float32)
        eig, _ = np.linalg.eig(cov)
        #print zc, yc, xc, '------', (z0+z1)/2.0, (y0+y1)/2.0, (x0+x1)/2.0

        weight_sum = np.sum(prob_roi)
        UNIT = SPACING * SPACING * SPACING
        prob_sum = weight_sum * UNIT

        eig = sorted(list(eig), reverse=True)
        #print eig
        #print weight_sum, np.linalg.det(cov), eig
        #print za, ya, xa
        #print cov

        pos = (zc/Z, yc/Y, xc/X)
        nodules_a.append([prob_sum, pos, [prob_sum, math.atan2(eig[0], eig[2])]])
        fts_roi = fts[z0:z1,y0:y1,x0:x1,:]
        fts_sum = np.sum(fts_roi, axis=(0,1,2))

        nodules_b.append((prob_sum, pos, list(fts_sum/weight_sum)))
        pass
    nodules_a = sorted(nodules_a, key=lambda x: -x[0])
    nodules_b = sorted(nodules_b, key=lambda x: -x[0])
    with open(os.path.join(ROOT, '%da' % view, uid + '.pkl'), 'wb') as f:
        pickle.dump((dim_a, nodules_a), f)
    with open(os.path.join(ROOT, '%db' % view, uid + '.pkl'), 'wb') as f:
        pickle.dump((dim_b, nodules_b), f)
    pass

def extract_views (ROOT, uid, r, mask):
    fts = r[0]
    fts = np.clip(fts, 0, 6)
    v1 = np.ascontiguousarray(r[1])
    v2 = np.ascontiguousarray(r[2])
    v3 = np.ascontiguousarray(r[3])
    if mask:
        v1 *= mask.images
        v2 *= mask.images
        v3 *= mask.images
    extract_view(ROOT, uid, 1, v1, fts)
    extract_view(ROOT, uid, 2, v2, fts)
    extract_view(ROOT, uid, 3, v3, fts)
    v12 = np.minimum(v1, v2)
    v13 = np.minimum(v1, v3)
    v23 = np.minimum(v2, v3)
    extract_view(ROOT, uid, 4, v12, fts)
    extract_view(ROOT, uid, 5, v13, fts)
    extract_view(ROOT, uid, 6, v23, fts)
    v123 = np.minimum(v12, v3)
    extract_view(ROOT, uid, 7, v123, fts)
    pass

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('prob', 'unet', 'prob model')      # prob model
#original default is luna.ns.3c
flags.DEFINE_string('fts', 'tcia', 'fts model')                # ft model
flags.DEFINE_string('mask', None, 'mask')
flags.DEFINE_integer('channels', 3, '')
flags.DEFINE_integer('stride', 16, '')
flags.DEFINE_integer('dilate', 30, '')
flags.DEFINE_bool('prob_dropout', False, '')
flags.DEFINE_bool('fts_dropout', True, '')

#BUFFER = 5
pre_queue = Queue.Queue(2)     # uid, case, mask, load_time
post_queue = Queue.Queue(2)

PRE_DONE = False
def pre_thread_worker (ROOT):
    global PRE_DONE
    uids = list(UIDS)[:10]
    for uid in uids:
        cache = os.path.join(ROOT, 'touch', uid)
        if os.path.exists(cache):
            continue
        with open(cache, 'wb') as f:
            pass
        start_time = time.time()
        case = load_8bit_lungs_noseg(uid)
        mask = None
        if not FLAGS.mask is None:
            try:
                mask_path = 'maskcache/%s/%s.npz' % (FLAGS.mask, uid)
                mask = load_mask(mask_path)
                mask = case.copy_replace_images(mask.astype(dtype=np.float32))
                mask = mask.rescale3D(SPACING)
                mask.round_stride(FLAGS.stride)
                if FLAGS.dilate > 0:
                    #print 'dilate', FLAGS.dilate
                    ksize = FLAGS.dilate * 2 + 1
                    mask.images = grey_dilation(mask.images, size=(ksize, ksize, ksize), mode = 'constant')
            except:
                traceback.print_exc()
                logging.error('failed to load mask %s' % mask_path)
                mask = None
        case = case.rescale3D(SPACING)
        case.round_stride(FLAGS.stride)
        load_time = time.time() - start_time
        pre_queue.put((uid, case, mask, load_time))
        pass
    PRE_DONE=True
    print "LOAD DONE"

TF_DONE = False
def gpu_thread_worker ():
    global TF_DONE
    model = Model(FLAGS.prob, FLAGS.fts, FLAGS.channels, FLAGS.prob_dropout, FLAGS.fts_dropout)
    config = tf.ConfigProto()
    #setGpuConfig(config)
    with tf.Session(config=config) as sess:
        tf.global_variables_initializer().run()
        model.load(sess)
        while True:
            try:
                job = pre_queue.get(True, 5)
            except Queue.Empty:
                if PRE_DONE:
                    break
                print "GPU THREAD STALL..."
                continue
            uid, case, mask, load_time = job
            start_time = time.time()
            r = model.apply(sess, case)
            done_time = time.time() - start_time
            post_queue.put((uid, r, mask, load_time, done_time))
        pass
    TF_DONE = True
    #print "TF DONE"

last_time = time.time()
def extract_thread_worker (ROOT):
    global last_time
    while True:
        try:
            job = post_queue.get(True, 5)
        except Queue.Empty:
            if TF_DONE:
                break
            #print "EXTRACT THREAD STALL..."
            continue
        uid, r, mask, load_time, predict_time = job
        start_time = time.time()
        extract_views(ROOT, uid, r, mask)
        cur_time = time.time()
        extract_time = cur_time - start_time
        print uid, load_time, predict_time, extract_time, (cur_time - last_time)
        last_time = cur_time
        pass
    #print "EXTRACT DONE"
    pass

def main (argv):
    name = FLAGS.prob
    if FLAGS.fts:
        name += '_' + FLAGS.fts
    if FLAGS.channels != 3:
        name += '_c' + str(FLAGS.channels)
    if not FLAGS.mask is None:
        name += '_' + FLAGS.mask
    if FLAGS.dilate != 30:
        name += '_d' + str(FLAGS.dilate)
    if SPACING != 0.8:
        name += '_s%.1f' % SPACING
    if GAP != 5:
        name += '_g%d' % GAP

    #name = '%s_%s_%d_%d' % (FLAGS.prob, FLAGS.fts, FLAGS.mode, FLAGS.channels)
    ROOT = os.path.join('cache', name)
    try_mkdir(os.path.join(ROOT, 'touch'))
    for view in range(1, 8):
        try_mkdir(os.path.join(ROOT, '%da' % view))
        try_mkdir(os.path.join(ROOT, '%db' % view))

    pre_thread = threading.Thread(target=pre_thread_worker, args=(ROOT,))
    pre_thread.start()
    gpu_thread = threading.Thread(target=gpu_thread_worker, args=())
    gpu_thread.start()
    extract_thread1 = threading.Thread(target=extract_thread_worker, args=(ROOT,))
    extract_thread1.start()
    extract_thread2 = threading.Thread(target=extract_thread_worker, args=(ROOT,))
    extract_thread2.start()

    pre_thread.join()
    gpu_thread.join()
    extract_thread1.join()
    extract_thread2.join()
    pass

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    tf.app.run()

