#!/usr/bin/env python
import sys
import random
import glob
import subprocess
from tqdm import tqdm
from adsb3 import *
import cv2
import picpac

MAX=None

try_mkdir('db/bg')
try_remove('db/bg/axial')
try_remove('db/bg/sagittal')
try_remove('db/bg/coronal')
axial_db = picpac.Writer('db/bg/axial')
sagittal_db = picpac.Writer('db/bg/sagittal')
coronal_db = picpac.Writer('db/bg/coronal')

def dump (case, db):
    N = case.images.shape[0]
    for o in range(0, N, 2):
        image = get3c(case.images, o)
        if image is None:
            continue
        buf = cv2.imencode('.png', image)[1].tostring()
        db.append(0, buf)
        pass
	pass

uids = [uid for uid, label in BOWL.train1 if label == 0]
if not MAX is None:
    random.shuffle(uids)
    uids = uids[:MAX]

for uid in uids:
    print uid
    case = load_case(uid)
    case.standardize_color()
    case = case.rescale3D(SPACING)
    dump(case, axial_db)
    dump(case.transpose(SAGITTAL), sagittal_db)
    dump(case.transpose(CORONAL), coronal_db)
    pass

