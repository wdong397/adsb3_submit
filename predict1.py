#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import sys
import argparse
import math
import datetime
import numpy as np
from adsb3 import *
from sklearn.model_selection import KFold
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss
import xgboost as xgb

#  0.43522
SPLIT=3
MIN_NODULE_SIZE=1
models = ['unet_k_b8',
          'unet_m2_b8',
          'unet_m4_b8',
          'unet_k_ft_b8',
         ]
PARTITIONS = [(1,1,1),(1,1,2),(3,1,1),(1,1,3)]
MAX_IT = 1000
model = xgb.XGBClassifier(n_estimators=MAX_IT,
            learning_rate=0.01,
            max_depth=2,seed=2016,
            subsample=0.9,
            colsample_bytree=0.4) #, reg_lambda=0.5)

xgb_param = {'max_depth':2,
         'eta':0.01,
         'silent':1,
         'objective':'binary:logistic',
         'subsample': 0.90,
         'colsample_bytree': 0.40,
         }


#model = GradientBoostingClassifier(n_estimators=50, learning_rate=0.1,  max_depth=2, random_state=0)
#model = XGBClassifier(n_estimators=100, learning_rate=0.1,max_depth=2,seed=2016)
#model = GradientBoostingClassifier(n_estimators=1200, learning_rate=0.01,  max_depth=2, random_state=2016)    # 313-0931
#model = XGBClassifier(n_estimators=300, learning_rate=0.05,max_depth=2,seed=2016, subsample = 0.90, colsample_bytree = 0.40, reg_lambda=1 ) #, reg_lambda=2)
#model = RandomForestClassifier(n_estimators=1000,max_depth=5)
#model = GradientBoostingClassifier(n_estimators=500, learning_rate=0.1,  max_depth=2, random_state=0)    # 313-0931

parser = argparse.ArgumentParser(description='')
parser.add_argument('--valid', action='store_true')
parser.add_argument('--cv', action='store_true')
args = parser.parse_args()

TRAIN = BOWL.train

failed = []
missing = []

TOTAL_PARTS = 0
for x, y, z in PARTITIONS:
    TOTAL_PARTS += x * y * z
    pass
print("TOTAL_PARTS: ", TOTAL_PARTS)

def extract (dim, nodules):
    if len(nodules) == 0 or nodules[0][0] < MIN_NODULE_SIZE:
        return [0] * dim
    else:
        return nodules[0][2][:dim]

def pyramid (dim, nodules):
    parts = []
    for _ in range(TOTAL_PARTS):
        parts.append([])
    for w, pos, ft in nodules:
        z, y, x = pos
        off = 0
        for LZ, LY, LX in PARTITIONS:
            zi = min(int(math.floor(z * LZ)), LZ-1)
            yi = min(int(math.floor(y * LY)), LY-1)
            xi = min(int(math.floor(x * LX)), LX-1)
            pi = off + (zi * LY + yi) * LX + xi
            off += LZ * LY * LX
            assert pi < off
            parts[pi].append((w, pos, ft))
            pass
        assert off == TOTAL_PARTS
        pass
    ft = []
    for nodules in parts:
        ft.extend(extract(dim, nodules))
        pass
    return ft

def load_all_ft (uid):
    ft = []

    for model in models:
        cached = os.path.join('cache', model, uid + '.pkl')
        if not os.path.exists(cached):
            missing.append(cached)
            ft.append(None)
            continue
        try:
            dim, nodules = load_fts(cached)
            ft.append(pyramid(dim, nodules))
        except:
            failed.append(cached)
            ft.append(None)
            pass
        pass
    return ft

U = []
X = []
Y = []
Xt = []

all_fts = []
for uid, label in TRAIN:
    fts = load_all_ft(uid)
    all_fts.append((uid, label, fts))

DIMS = [None] * len(models)
# fix None features
for i in range(len(models)):
    dim = 0
    for _, _, fts in all_fts:
        if not fts[i] is None:
            dim = len(fts[i])
            break
        pass
    DIMS[i] = dim
    pass

def merge_fts (fts):
    v = []
    fixed = False
    for i in range(len(fts)):
        if fts[i] is None:
            fixed = True
            v.extend([0] * DIMS[i])
        else:
            assert len(fts[i]) == DIMS[i]
            v.extend(fts[i])
            pass
        pass
    return v, fixed

for uid, label, fts in all_fts:
    v, fixed = merge_fts(fts)
    if args.valid and fixed:
        continue
    U.append(uid)
    Y.append(label)
    X.append(v)

for uid, _ in BOWL.test:
    v, fixed = merge_fts(load_all_ft(uid))
    Xt.append(v)

X = np.array(X, dtype=np.float32)
Y = np.array(Y, dtype=np.float32)
Xt = np.array(Xt, dtype=np.float32)
print('X', X.shape)
print('Y', Y.shape)
print('Xt', Xt.shape)

Y1 = np.sum(Y)
Y0 = len(Y) - Y1
print('neg:', Y0, 'pos:', Y1)

kf = KFold(n_splits=SPLIT, shuffle=True, random_state=88)
y_pred = Y * 0
y_pred_prob = Y * 0
Yt = np.zeros((Xt.shape[0],), dtype=np.float32)

def pred_wrap (Xin):
    Yout = model.predict_proba(Xin)[:,1]
    return Yout

N = 0
for train, test in kf.split(X):
    X_train, X_test, y_train = X[train,:], X[test,:], Y[train]
    model.fit(X_train, y_train)
    y_pred[test] = model.predict(X_test)
    y_pred_prob[test] = pred_wrap(X_test)
    N += 1

model.fit(X, Y)
Yt = pred_wrap(Xt)

if args.cv:
    xgb.cv(xgb_param, xgb.DMatrix(X, label=Y), (MAX_IT+500),
           nfold=10,
           stratified=True,
           metrics={'logloss'},
           callbacks=[xgb.callback.print_evaluation(show_stdv=False)])

try:
    print(classification_report(Y, y_pred, target_names=["0", "1"]))
except:
    pass
print("logloss",log_loss(Y, y_pred_prob))
print("%d corrupt" % len(failed))
for uid in failed:
    print(uid)
    pass
pass
assert N == SPLIT

print("%d missing" % len(missing))
if args.valid:
    sys.exit(0)

for uid in missing:
    print(uid)
    pass
pass

test_uids = [uid for uid, _ in BOWL.test]
test_ys = list(Yt)

test_meta = zip(test_uids, test_ys)

prefix = os.path.join('stage', datetime.datetime.now().strftime('%m%d-%H%M%S')[1:])
if os.path.exists(prefix + '.submit'):
    print("submission %s already exists, not overwriting." % prefix)
    sys.exit(0)
dump_meta(prefix + '.submit', test_meta)

eval_ref(test_meta)
with open(prefix + '.cmd', 'w') as f:
    f.write(' '.join(sys.argv))
    f.write('\n')

COLS = [i+1 for i in range(X.shape[1])]

def libsvm_row (y, x):
    return '%g %s\n' % (y,
            ' '.join(['%d:%.6f' % (c, v) for c, v in zip(COLS, list(x))]))

with open(prefix + '.train', 'w') as f:
    for i in range(len(Y)):
        f.write(libsvm_row(Y[i], list(X[i])))
with open(prefix + '.test', 'w') as f:
    for i in range(Xt.shape[0]):
        f.write(libsvm_row(0, list(Xt[i])))

# generate bad cases
with open(prefix + '.html', 'w') as f:
    f.write('<html><body><table><tr><th>class</th><th>prob</th><th>uid</th></tr>\n')
    v = zip(Y, list(Y * y_pred_prob + (1-Y)*(1- y_pred_prob)), U, X)
    v = sorted(v, key=lambda x: x[1])
    for y, p, uid, x in v:
        ft_txt = ' '.join(['%.4f' % ft for ft in x])
        f.write('<tr><td>%d</td><td>%.4f</td><td><a target="_blank" href="http://localhost/~wdong/lungs/%s.html">%s</a></td><td>%s</td></tr>\n' % (y, p, uid, uid, ft_txt))
        pass
    f.write('</table></body></html>\n')

print('Submission generated:', prefix + '.submit')

