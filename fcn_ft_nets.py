#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim.nets import resnet_v1
from tensorflow.contrib.slim.nets import resnet_utils

def tiny (X, num_classes=2, num_ft=9):
    # stride is  2 * 2 * 2 * 2 = 16
    net = X
    layers = [X]
    with tf.name_scope('tiny'):
        net = slim.conv2d(net, 64, 3, 2, scope='conv1')
        net = slim.max_pool2d(net, 2, 2, scope='pool1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_2')
        net = slim.max_pool2d(net, 2, 2, scope='pool2')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_1')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_2')
        net = slim.conv2d(net, 128, 1, 1, scope='conv5')
        #net = slim.dropout(net, keep_prob=0.9, scope='dropout')
        net1 = slim.conv2d(net, 32, 1, 1, scope='conv6_1',
                            activation_fn=None,
                            normalizer_fn=None,
                         )
        net2 = slim.conv2d(net, 64, 1, 1, scope='conv6_2',
                            activation_fn=None,
                            normalizer_fn=None,
                         )
        net1 = slim.conv2d_transpose(net1, num_classes, 17, 8, scope='upscale1')
        net2 = slim.conv2d_transpose(net2, num_ft, 17, 8, scope='upscale2')
    net1 = tf.identity(net1, 'logits')
    net2 = tf.identity(net2, 'fts')
    return net1, net2, 16

def tinyX2_upscale (net, name, channels=1): 
    with tf.name_scope(name):
        # 16 x upscale
        net = slim.conv2d_transpose(net, 256, 5, 2)
        net = slim.conv2d_transpose(net, 128, 5, 2)
        net = slim.conv2d_transpose(net, 64, 5, 2)
        net = slim.conv2d_transpose(net, 32, 5, 2)
        net = slim.conv2d(net, 32, 3, 1, scope=name+'up5')
        net = slim.conv2d(net, channels, 1, 1, scope=name+'up6', activation_fn=None, normalizer_fn=None)
    return net

def tinyX2 (X, KEEP=None, num_classes=2, num_ft=9):
    # stride is  2 * 2 * 2 * 2 = 16
    net = X
    layers = [X]
    with tf.name_scope('tinyX2'):
        # slim.arg_scope([slim.conv2d]):
        # slim.conv2d defaults:
        #   padding = 'SAME'
        #   activation_fn = nn.relu
        # parameters: net, out_channels, kernel_size, stride
        net = slim.conv2d(net, 64, 3, 2, scope='conv1')
        net = slim.max_pool2d(net, 2, 2, scope='pool1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_2')
        net = slim.max_pool2d(net, 2, 2, scope='pool2')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_1')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_2')
        net = slim.max_pool2d(net, 2, 2, scope='pool3')
        net = slim.conv2d(net, 512, 3, 1, scope='conv4_1')
        net = slim.conv2d(net, 512, 3, 1, scope='conv4_2')
        if not KEEP is None:
            net = slim.dropout(net, keep_prob=KEEP)

        chs = []
        for i in range(num_ft):
                chs.append(tinyX_upscale(net, 'upscale_%d'%i))
        net2 = tf.concat(3, chs)
        net1 = tinyX_upscale(net, 'upscale_prob', channels=2)
    net1 = tf.identity(net1, 'logits')
    net2 = tf.identity(net2, 'fts')
    return net1, net2, 16

def tinyX_upscale (net, name, channels=1): 
    with tf.name_scope(name):
        # 16 x upscale
        net = slim.conv2d(net, 64, 1, 1)
        net = slim.conv2d(net, 32, 1, 1)
        net = slim.conv2d_transpose(net, 16, 8, 4)
        net = slim.conv2d_transpose(net, channels, 8, 4, activation_fn=None, normalizer_fn=None)
        #net = slim.conv2d_transpose(net, 64, 8, 4)
        #net = slim.conv2d_transpose(net, 64, 8, 4)
        #net = slim.conv2d_transpose(net, 16, 1, 1)
        #net = slim.conv2d(net, channels, 1, 1, activation_fn=None, normalizer_fn=None)
    return net

def tinyX (X, num_classes=2, num_ft=9, keep=None):
    # stride is  2 * 2 * 2 * 2 = 16
    net = X
    layers = [X]
    with tf.name_scope('tinyX'):
        # slim.arg_scope([slim.conv2d]):
        # slim.conv2d defaults:
        #   padding = 'SAME'
        #   activation_fn = nn.relu
        # parameters: net, out_channels, kernel_size, stride
        net = slim.conv2d(net, 64, 3, 2, scope='conv1')
        net = slim.max_pool2d(net, 2, 2, scope='pool1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_2')
        net = slim.max_pool2d(net, 2, 2, scope='pool2')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_1')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_2')
        net = slim.max_pool2d(net, 2, 2, scope='pool3')
        net = slim.conv2d(net, 256, 3, 1, scope='conv4_1')
        net = slim.conv2d(net, 256, 1, 1, scope='conv4_2')
        if not keep is None:
            net = slim.dropout(net, keep_prob=keep)
        chs = []
        for i in range(num_ft):
                chs.append(tinyX_upscale(net, 'upscale_%d'%i))
        net2 = tf.concat(3, chs)
        net1 = tinyX_upscale(net, 'upscale_prob', channels=2)
    net1 = tf.identity(net1, 'logits')
    net2 = tf.identity(net2, 'fts')
    return net1, net2, 16

def tinyX3_upscale (net, name, channels, shape): 
    with tf.name_scope(name):
        # 16 x upscale
        net = slim.conv2d(net, 128, 1, 1)
        net = slim.conv2d(net, 32, 1, 1)
        net = tf.image.resize_images(net, shape*16, method=tf.image.ResizeMethod.BILINEAR)
        net = slim.conv2d(net, 16, 3, 1, activation_fn=None, normalizer_fn=None)
        net = slim.conv2d(net, channels, 3, 1, activation_fn=None, normalizer_fn=None)
    return net

def tinyX3 (X, num_classes=2, num_ft=7, keep=None):
    # stride is  2 * 2 * 2 * 2 = 16
    net = X
    layers = [X]
    with tf.name_scope('tinyX'):
        # slim.arg_scope([slim.conv2d]):
        # slim.conv2d defaults:
        #   padding = 'SAME'
        #   activation_fn = nn.relu
        # parameters: net, out_channels, kernel_size, stride
        net = slim.conv2d(net, 32, 3, 1)
        net = slim.max_pool2d(net, 2, 2)
        net = slim.conv2d(net, 64, 3, 1)
        net = slim.conv2d(net, 64, 3, 1)
        net = slim.max_pool2d(net, 2, 2)
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.max_pool2d(net, 2, 2)
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.max_pool2d(net, 2, 2)
        net = slim.conv2d(net, 512, 3, 1)
        net = slim.conv2d(net, 512, 3, 1)
        #net = slim.conv2d(net, 256, 1, 1)
        if not keep is None:
            net = slim.dropout(net, keep_prob=keep)
        chs = []
        shape = tf.slice(tf.shape(net), [1], [2])
        for i in range(num_ft):
            chs.append(tinyX3_upscale(net, 'upscale_%d'%i, 1, shape))
        net2 = tf.concat(3, chs)
        net1 = tinyX3_upscale(net, 'upscale_prob', 2, shape)
    net1 = tf.identity(net1, 'logits')
    net2 = tf.identity(net2, 'fts')
    return net1, net2, 16


# conv2d and conv2d_transpose

# conv2d output size if padding = 'SAME':   W <- (W + S -1)/S 
#                                 'VALID':  W <- (W - F + S)/S
def simple (X, num_classes=2):
    # stride is  2 * 2 * 2 * 2 = 16
    net = X
    layers = [X]
    with tf.name_scope('simple'):
        # slim.arg_scope([slim.conv2d]):
        # slim.conv2d defaults:
        #   padding = 'SAME'
        #   activation_fn = nn.relu
        # parameters: net, out_channels, kernel_size, stride
        net = slim.conv2d(net, 100, 5, 2, scope='conv1')
        net = slim.max_pool2d(net, 2, 2, scope='pool1')
        net = slim.conv2d(net, 200, 5, 2, scope='conv2')
        net = slim.max_pool2d(net, 2, 2, scope='pool2')
        net = slim.conv2d(net, 300, 3, 1, scope='conv3')
        net = slim.conv2d(net, 300, 3, 1, scope='conv4')
        net = slim.dropout(net, keep_prob=0.9, scope='dropout')
        net = slim.conv2d(net, 20, 1, 1, scope='layer5')
        net = slim.conv2d_transpose(net, num_classes, 31, 16, scope='upscale')
    net = tf.identity(net, 'logits')
    return net, 16

def  resnet_v1_50 (X, num_classes=2):
    with tf.name_scope('resnet_v1_50'):
        net, _ = resnet_v1.resnet_v1_50(X,
                                num_classes=num_classes,
                                global_pool = False,
                                output_stride = 16)
        net = slim.conv2d_transpose(net, num_classes, 31, 16, scope='upscale')
    net = tf.identity(net, 'logits')
    return net, 16

def resnet_tiny (inputs, num_classes=2, scope ='resnet_tiny'):
    blocks = [ 
        resnet_utils.Block('block1', resnet_v1.bottleneck,
                           [(64, 32, 1)] + [(64, 32, 2)]),
        resnet_utils.Block('block2', resnet_v1.bottleneck,
                           [(128, 64, 1)] + [(128, 64, 2)]),
        resnet_utils.Block('block3', resnet_v1.bottleneck,
                           [(256, 64, 1)] + [(128, 64, 2)]),
        resnet_utils.Block('block4', resnet_v1.bottleneck, [(128, 64, 1)])
    	]   
    net,_ = resnet_v1.resnet_v1(
        inputs, blocks,
        # all parameters below can be passed to resnet_v1.resnet_v1_??
        num_classes = None,       # don't produce final prediction
        global_pool = False,       # produce 1x1 output, equivalent to input of a FC layer
        output_stride = 16,
        include_root_block=True,
        reuse=False,              # do not re-use network
        scope=scope)
    net = slim.conv2d_transpose(net, num_classes, 31, 16, scope='upscale')
    net = tf.identity(net, 'logits')
    return net, 16


def tinyZ_upscale (net, name, channels=1): 
    with tf.name_scope(name):
        net = slim.conv2d(net, 64, 3, 1)
        # 16 x upscale
        net = slim.conv2d_transpose(net, 16, 9, 4)
        net = slim.conv2d_transpose(net, channels, 9, 4, activation_fn=None, normalizer_fn=None)
    return net

def tinyZ (X, KEEP=None, num_classes=2, num_ft=9):
    # stride is  2 * 2 * 2 * 2 = 16
    net = X
    layers = [X]
    with tf.name_scope('tinyZ'):
        # slim.arg_scope([slim.conv2d]):
        # slim.conv2d defaults:
        #   padding = 'SAME'
        #   activation_fn = nn.relu
        # parameters: net, out_channels, kernel_size, stride
        net = slim.conv2d(net, 64, 3, 2, scope='conv1')
        net = slim.max_pool2d(net, 2, 2, scope='pool1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_2')
        net = slim.max_pool2d(net, 2, 2, scope='pool2')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_1')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_2')
        net = slim.max_pool2d(net, 2, 2, scope='pool3')
        net = slim.conv2d(net, 512, 3, 1, scope='conv4_1')
        net = slim.conv2d(net, 512, 3, 1, scope='conv4_2')
        if not KEEP is None:
            net = slim.dropout(net, keep_prob=KEEP)

        chs = []
        for i in range(num_ft):
                chs.append(tinyZ_upscale(net, 'upscale_%d'%i))
        net2 = tf.concat(3, chs)
        net1 = tinyZ_upscale(net, 'upscale_prob', channels=2)
    net1 = tf.identity(net1, 'logits')
    net2 = tf.identity(net2, 'fts')
    return net1, net2, 16

def smallft_upscale (net, name, channels=1): 
    with tf.name_scope(name):
        # 16 x upscale
        net = slim.conv2d(net, 64, 3, 1)
        #net = slim.conv2d(net, 32, 1, 1)
        net = slim.conv2d_transpose(net, 16, 5, 2)
        net = slim.conv2d_transpose(net, channels, 9, 4, activation_fn=None, normalizer_fn=None)
        #net = slim.conv2d_transpose(net, 64, 8, 4)
        #net = slim.conv2d_transpose(net, 64, 8, 4)
        #net = slim.conv2d_transpose(net, 16, 1, 1)
        #net = slim.conv2d(net, channels, 1, 1, activation_fn=None, normalizer_fn=None)
    return net

def smallft (X, num_classes=2, num_ft=9):
    # stride is  2 * 2 * 2 * 2 = 16
    net = X
    layers = [X]
    with tf.name_scope('smallft'):
        # slim.arg_scope([slim.conv2d]):
        # slim.conv2d defaults:
        #   padding = 'SAME'
        #   activation_fn = nn.relu
        # parameters: net, out_channels, kernel_size, stride
        net = slim.conv2d(net, 64, 3, 2, scope='conv1')
        net = slim.max_pool2d(net, 2, 2, scope='pool1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_1')
        net = slim.conv2d(net, 128, 3, 1, scope='conv2_2')
        net = slim.max_pool2d(net, 2, 2, scope='pool2')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_1')
        net = slim.conv2d(net, 256, 3, 1, scope='conv3_2')
        chs = []
        for i in range(num_ft):
                chs.append(smallft_upscale(net, 'upscale_%d'%i))
        net2 = tf.concat(3, chs)
    net2 = tf.identity(net2, 'fts')
    return net2, 8
